# Hands on Lab Exercise:Using Stored Procedure from R

## Objective:

In this lab we will use RStudio workbench to create and querying data in MySQl database.

## Pre-Requisite:

1.	MySQL

2.	RStudio

## Contents:

1.	Installation of RStudio.

2.	Launching MySQL and RStudio workbench.

3.	Load the Libraries.

4.	Connect to database.

5.	Creating Stored Procedure.

## Installation of RStudio.

You can download the RStudio free form the link: [Download](https://rstudio.com/products/rstudio/download/).

Here in this site you can download the free version.

## Launching MySQL and RStudio workbench.

Kindly open the MySQl workbench and Command line client.

<img src="images/37.png"  width="600">

<img src="images/38.png"  width="600">

After that open the Rstudio workbench.

<img src="images/39.png"  width="600">

Click on **File -> New File -> R Script**.

This opens New File where we can work.

<img src="images/40.png"  width="600">

**Load the Libraries.**

Kindly Enter the following query:

**install.packages("RMySQL")**

**library(DBI)**

**library(RMySQL)**

This will install the libraries needed for connecting the database.

<img src="images/41.png"  width="600">

**Note: To run the script, select the query to run and click the run button on top-left corner. Kindly check the console panel below the file to know whether the query executed successfully or not.**

**Connect to Database.**

Run the below query:

**mydb = dbConnect(MySQL(), user='root', password='******', dbname='world', host='localhost')**

Here kindly enter the password used to open MySQL. In dbname we have selected ‘world’ database form the default databases. You can select any of them, here we have selected ‘world’ database.

This completes the connection setup.

<img src="images/42.png"  width="600">

**Creating Stored Procedure**

Now we will run some queries inside the database.

Let’s check the number of tables inside ‘world’ database.

**dbListTables(mydb)**

<img src="images/43.png"  width="600">

Output:

<img src="images/44.png"  width="600">

You can see the output in the console panel. There are 3 tables: **‘city’, ‘country’, ‘countrylanguage’, ‘testtable’.**

Now we will create a Stored Procedure to call a function and execute that Procedure.

Firstly, lets create the Procedure:

**query<-"CREATE PROCEDURE world.Tester() BEGIN SELECT * FROM city; END"**

**rs = dbSendQuery(mydb, query)**

Now Execute the Query.

**result=dbSendQuery(mydb, "CALL world.Tester();")**

**data = fetch(result, n=-1)**

**head(data)**

<img src="images/45.png"  width="600">

Output:

<img src="images/46.png"  width="600">

<img src="images/47.png"  width="600">

                                                 **This Ends the Exercise**






































