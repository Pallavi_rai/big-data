# Hands on Lab Exercise:In-Database Analytics with R

## Objective:

In this lab we will use RStudio workbench to make a K-means cluster on MySql Database.

## Pre-Requisite:

1.	MySQL

2.	RStudio

## Contents:

1.	Installation of RStudio.

2.	Launching MySQL and RStudio workbench.

3.	Load the Libraries.

4.	Connect to database.

5.	Creation of K-Means Cluster.

## Installation of RStudio.

You can download the RStudio free form the link: [Download](https://rstudio.com/products/rstudio/download/).

Here in this site you can download the free version.

## Launching MySQL and RStudio workbench. 

Kindly open the MySQl workbench and Command line client.

<img src="images/48.png"  width="600">

<img src="images/49.png"  width="600">

After that open the Rstudio workbench.

<img src="images/50.png"  width="600">

Click on **File -> New File -> R Script.**

This opens New File where we can work.

<img src="images/51.png"  width="600">

**Load the Libraries.**

Load all the libraries needed in this lab.

**install.packages('tidyverse')**

**install.packages('factoextra')**

**library(tidyverse)  # data manipulation**

**library(cluster)    # clustering algorithms**

**library(factoextra)   # clustering algorithms & visualization**

**install.packages("RMySQL")**

**library(DBI)**

**library(RMySQL)**

<img src="images/52.png"  width="600">

<img src="images/53.png"  width="600">

After loading all the libraries, read the .csv file whose data we will use to do the analysis.

**df<- read.csv('USArrests.csv')**

<img src="images/54.png"  width="600">

<img src="images/55.png"  width="600">

**Connect to Database.**

Run the below query:

**mydb = dbConnect(MySQL(), user='root', password='******', dbname='world', host='localhost')**

Here kindly enter the password used to open MySQL. In dbname we have selected ‘world’ database form the default databases. You can select any of them, here we have selected ‘world’ database.

This completes the connection setup.

<img src="images/56.png"  width="600">

<img src="images/57.png"  width="600">

**Creation of K-Means Cluster**

Here we will first create a table, **“testable”** and then we will load the.csv file we read in previous step into this table.

**dbListTables(mydb)**

**dbWriteTable(mydb, "Testtable", df)**

**rs=dbSendQuery(mydb, "Select * from testtable")**

**df=fetch(rs, n=-1)**

**head(df)**

**df<- df[-c(1)]**

**head(df)**

<img src="images/58.png"  width="600">

<img src="images/59.png"  width="600">

Now we will start the Analysis of the data. Copy the following code to execute the analysis.

**df <- na.omit(df)**

**rownames(df)<- df$X**

**df<-df[-c(1)]**

**df <- scale(df)**

**head(df)**

**k2 <- kmeans(df, centers = 2, nstart = 25)**

**k2**

**fviz_cluster(k2, data = df)**

**k3 <- kmeans(df, centers = 3, nstart = 25)**

**k4 <- kmeans(df, centers = 4, nstart = 25)**

**k5 <- kmeans(df, centers = 5, nstart = 25)**

**# plots to compare**

**p1 <- fviz_cluster(k2, geom = "point", data = df) + ggtitle("k = 2")**

**p2 <- fviz_cluster(k3, geom = "point",  data = df) + ggtitle("k = 3")**

**p3 <- fviz_cluster(k4, geom = "point",  data = df) + ggtitle("k = 4")**

**p4 <- fviz_cluster(k5, geom = "point",  data = df) + ggtitle("k = 5")**

**library(gridExtra)**

**grid.arrange(p1, p2, p3, p4, nrow = 2)**

**# Determinng Optimal Cluster**

**set.seed(123)**

**# function to compute total within-cluster sum of square**

**wss <- function(k) {**

**kmeans(df, k, nstart = 10 )$tot.withinss**

**}**

**# Compute and plot wss for k = 1 to k = 15**

**k.values <- 1:15**

**# extract wss for 2-15 clusters**

**wss_values <- map_dbl(k.values, wss)**

**plot(k.values, wss_values,**

   **type="b", pch = 19, frame = FALSE,** 

   **xlab="Number of clusters K",**

   **ylab="Total within-clusters sum of squares")**

**set.seed(123)**

**fviz_nbclust(df, kmeans, method = "wss")**

**#Exracting Results**

**set.seed(123)**

**final <- kmeans(df, 4, nstart = 25)**

**print(final)**

**fviz_cluster(final, data = df)**

**#extract the clusters and add to our initial data to do some descriptive statistics at the cluster level:**

**USArrests %>%**

  **mutate(Cluster = final$cluster) %>%**

  **group_by(Cluster) %>%**

  **summarise_all("mean")**

<img src="images/60.png"  width="600">

<img src="images/61.png"  width="600">

All Outputs:

<img src="images/62.png"  width="600">

<img src="images/63.png"  width="600">

<img src="images/64.png"  width="600">

<img src="images/65.png"  width="600">

<img src="images/66.png"  width="600">

<img src="images/67.png"  width="600">

<img src="images/68.png"  width="600">

<img src="images/69.png"  width="600">

<img src="images/70.png"  width="600">

                                            **This Ends the Exercise**





















