# Hands on Lab Exercise:Creating and Modifying Database Objects using R

## Objective:

In this lab we will use RStudio workbench to create and querying data in MySQl database.

## Pre-Requisite:

1.	MySQL

2.	RStudio

## Contents:

1.	Installation of RStudio.

2.	Launching MySQL and RStudio workbench.

3.	Load the Libraries.

4.	Connect to database.

5.	Execute a Query to create and updating tables in MySQL Database.

## Installation of RStudio.

You can download the RStudio free form the link: [Download](https://rstudio.com/products/rstudio/download/#download)

Here in this site you can download the free version.

## Launching MySQL and RStudio workbench. 

Kindly open the MySQl workbench and Command line client.

<img src="images/21.png"  width="600">

<img src="images/22.png"  width="600">

After that open the Rstudio workbench.

<img src="images/23.png"  width="600">

Click on **File -> New File -> R Script.**

This opens New File where we can work.

<img src="images/24.png"  width="600">

**Load the Libraries.**

Kindly Enter the following query:

**install.packages("RMySQL")**

**library(DBI)**

**library(RMySQL)**

This will install the libraries needed for connecting the database.

<img src="images/25.png"  width="600">

**Note: To run the script, select the query to run and click the run button on top-left corner. Kindly check the console panel below the file to know whether the query executed successfully or not.**

**Connect to Database.**

Run the below query:

**mydb = dbConnect(MySQL(), user='root', password='******', dbname='world', host='localhost')**

Here kindly enter the password used to open MySQL. In dbname we have selected ‘world’ database form the default databases. You can select any of them, here we have selected ‘world’ database.

This completes the connection setup.

<img src="images/26.png"  width="600">

## Execute a Query to create and updating tables in MySQL Database.

Now we will run some queries inside the database.

Let’s check the number of tables inside ‘world’ database.

**dbListTables(mydb)**

<img src="images/27.png"  width="600">

You can see the output in the console panel. There are 3 tables: **‘city’, ‘country’, ‘countrylanguage’**

Let’s take one of the tables called **‘city’** and provide a value to the table.

**df<-data.frame()**

**dbWriteTable(mydb, name='city', value=df)**

<img src="images/28.png"  width="600">

Now we will fetch the data for tabale ‘city’ and show the result using query:

**rs = dbSendQuery(mydb, "select * from city")**

**data = fetch(rs, n=-1)**

**data**

<img src="images/29.png"  width="600">

You can see the whole output either by maximize the console panel or scroll up.

We can query the database tables in MySql using the function **dbSendQuery()**. The query gets executed in MySql and the result set is returned using the R **fetch()** function. Finally it is stored as a data frame in R.

**result = dbSendQuery(mydb, "select * from city")**

**data.frame = fetch(result, n = 5)**

**print(data.frame)**

<img src="images/30.png"  width="600">

<img src="images/31.png"  width="600">

We can pass any valid select query to get the result.

**result = dbSendQuery(mydb, "Select * from city where CountryCode = 'AFG'")**

**data.frame = fetch(result, n = -1)**

**print(data.frame)**

<img src="images/32.png"  width="600">

Inserting Data into Tables

**dbSendQuery(mydb, "insert into city(Name, CountryCode, District, Population) values('Afghanistan', 'AFG', 'Argo', 125800)"**

<img src="images/33.png"  width="600">

<img src="images/36.png"  width="600">

We can create tables in the MySql using the function **dbWriteTable()**. It overwrites the table if it already exists and takes a data frame as input.

**dbWriteTable(mydb, name='Continent, value = df)**

<img src="images/34.png"  width="600">

We can drop the tables in MySql database passing the drop table statement into the dbSendQuery() in the same way we used it for querying data from tables.

**dbSendQuery(mydb, 'drop table if exists Continent')**

<img src="images/35.png"  width="600">

                                            **This Ends the Exercise.**






































