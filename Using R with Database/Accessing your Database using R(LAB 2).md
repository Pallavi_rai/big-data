# Hands on Lab Exercise:Accessing your Database using R

## Objective:

In this lab we will use RStudio workbench to access data in MySQl database using RJDBC.

## Pre-Requisite:

1.	MySQL

2.	RStudio

## Contents:

1.	Installation of RStudio.

2.	Launching MySQL and RStudio workbench.

3.	Load the Libraries.

4.	Connect to database.

5.	Execute a Query and retrieve results.

### Installation of RStudio.

You can download the RStudio free form the link: [Download RStudio](https://rstudio.com/products/rstudio/download/)

Here in this site you can download the free version.

### Launching MySQL and RStudio workbench. 

Kindly open the MySQl workbench and Command line client.

<img src="images/12.png"  width="600">

<img src="images/13.png"  width="600">

After that open the Rstudio workbench.

<img src="images/14.png"  width="600">

Click on **File -> New File -> R Script.**

This opens New File where we can work.

<img src="images/15.png"  width="600">

**Load the Libraries.**

Kindly Enter the following query:

**install.packages("RMySQL")**

**library(DBI)**

**library(RMySQL)**

This will install the libraries needed for connecting the database.

<img src="images/16.png"  width="600">

**Note: To run the script, select the query to run and click the run button on top-left corner. Kindly check the console panel below the file to know whether the query executed successfully or not.**

**Connect to Database.**

Run the below query:

**mydb = dbConnect(MySQL(), user='root', password='******', dbname='world', host='localhost')**

Here kindly enter the password used to open MySQL. In dbname we have selected ‘world’ database form the default databases. You can select any of them, here we have selected ‘world’ database.

<img src="images/17.png"  width="600">

**Execute a Query and retrieve results.**

Now we will run some queries inside the database.

Let’s check the number of tables inside ‘world’ database.

**dbListTables(mydb)**

<img src="images/18.png"  width="600">

You can see the output in the console panel. There are 3 tables: **‘city’, ‘country’, ‘countrylanguage’**

Let’s take one of the tables called **‘city’** and provide a value to the table.

**df<-data.frame()**

**dbWriteTable(mydb, name='city', value=df)**

<img src="images/19.png"  width="600">

Now we will fetch the data for tabale ‘city’ and show the result using query:

**rs = dbSendQuery(mydb, "select * from city")**

**data = fetch(rs, n=-1)**

**data**

<img src="images/20.png"  width="600">

You can see the whole output either by maximize the console panel or scroll up.

**In the next lab we will try to Create and Query some Database Objects using R.**


                                                 **This Ends the Exercise.**















