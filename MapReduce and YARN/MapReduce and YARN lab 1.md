![s](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/skillup.png)

# **MapReduce and YARN**

## **Lab-1 Exercise**

### **Objective**

To run the wordcount program using MapReduce.

### **Pre-requisite**

**1.** Java 8

**2.** Hadoop 3.X

### **Prepare:**

**1.** Locate the MapReduce example.jar file in &quot;Hadoop&quot; Folder. Location will be:

**< Hadoop root folder >\share\hadoop\mapreduce\hadoop-mapreduce-examples-3.1.2**

**For example, C:\HADOOP\share\hadoop\mapreduce\hadoop-mapreduce-examples3.1.2**

2. We will use the [books.csv](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/lab%20files/books.csv) for this lab exercise. Kindly place csv in directory and note the
path

## **Let&#39;s run the commands now:**

**Step 1** - Open cmd in Administrative mode (Press Windows+R to open the “Run” box. Type “cmd” into the box and then press **Ctrl+Shift+Enter** to run the command as an administrator) and change working directory to **C:\HADOOP\sbin** and start cluster by using command. In Mac and Linux use `sudo start-all.sh`

**start-all.cmd**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/1.png"  width="600">

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/2.png"  width="600">

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/3.png"  width="600">

**Step 2** - Since we setup the path for the Hadoop bin directory in our path during the
installation, we can run the commands from anywhere in the command prompt.

**hadoop dfsadmin -safemode leave**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/5.png"  width="600">

**Step 3** - Create an empty input directory/folder in HDFS using below given command.

**hadoop fs -mkdir /input\_dir**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/6.png"  width="600">

**Step 4** - Copy the input .csv file named books.csv in the input directory(input\_dir) of HDFS.

**hadoop fs -put <<"path of the file with extension">> /input\_dir**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/7.png"  width="600">

**Step 5** - Verify that the input file is now available in HDFS directory(input\_dir).

**hadoop fs -ls /input\_dir**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/8.png"  width="600">

**Step 6** – You can verify the input file via browser also. Go to below given link

**http://localhost:9870/**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/had_43.png"  width="600">

**Step 7** – Here you can check the number of datanodes and namenodes also. Now to check the input file Go to **Utilities** on the top menu**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/had_43.png"  width="600">

**From the list of files and directories choose, input_dir to browse the directory.**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/pic_1.png"  width="600">

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/pic_2.png"  width="600">

**Step 8** – Now returning back to command line. Verify the content of the copied file.

**hadoop dfs -cat /input\_dir/<<"file\_name">>;**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/9.png"  width="600">

**Step 9** - Run the Mapreduce.jar file and provide input and output directories.

**hadoop jar <<"jar file path with extension">> wordcount /input\_dir /output\_dir**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/5.png"  width="600">

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/11.png"  width="600">

**Step 10** - Verify content of the generated output file.

**hadoop dfs -cat /output\_dir/\***

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/12.png"  width="600">

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/13.png"  width="600">

**Step 11** – You can verify the output file in browser.

**http://localhost:9870/**

Now to check the input file Go to **Utilities** on the top menu.

**Browse output_dir to see if part-r-0000 is listed.**

This is the final output file which can even be downloaded.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/pic_3.png"  width="600">

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/pic_4.png"  width="600">

**Step 12** - You can continue add any number of files and if you need to delete some files from the directory.

**hadoop fs -rm -r /input\_dir/name_of_file_to_delete**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/14.png"  width="600">

**Step 13** – To delete the input and output directory.

**hadoop fs -rm -r /input\_dir**

**hadoop fs -rm -r /ouput\_dir**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/15.png"  width="600">

**Step 14** – Stop all Hadoop resources before exit the command prompt using `stop-all.cmd`. In Mac and Linux use `sudo stop-all.sh`

**stop-all.cmd**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/16.png"  width="600">

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/17.png"  width="600">

**In the next lab we will run some more MapReduce Program.**

                                                              

                                                                        **This Ends the Exercise**

