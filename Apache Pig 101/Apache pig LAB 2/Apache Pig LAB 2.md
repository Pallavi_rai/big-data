![image](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/skillup.png)

# Lab 2: Hands-On activities on Grunt Shell:

In the previous lab, we have already installed **APACHE PIG** , and we were into the **GRUNT SHELL**.

### OBJECTIVE:
- is to make you familiar with basic Pig Operators and running commands from the grunt shell.

**LET'S RUN SOME COMMANDS NOW:**

**Step 1:** Execute Pig in local mode by running below command: 

```
pig -x local
```

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/images/pig_lab_1.png"  width="600">

**Step 2:** Download the lab file [employees.txt](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/lab%20files/employees.txt)


**Step 3:** After the file is downloaded, save the file in your local system. In this example it is stored in d:\ drive. Make a note of the **path** of the file on your system.

**Step 4:** Now, we will Load the file in a variable: input_file using the below command: 
(replace the path with your path)

```
input_file = LOAD 'd:/employees.txt' USING PigStorage(',')

as (Emp_ID: Int, Emp_Name:chararray, Emp_Location:chararray);
```

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/images/pig_lab_2.png"  width="600">


**Step 5:** Now we will check the results using the DUMP operator:

```
DUMP input_file;
```

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/images/pig_lab_3.png"  width="600">


<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/images/pig_lab_4.png"  width="600">


## Executing Pig script in Batch Mode: 

We can also execute Pig Script in Batch Mode.

**Step 1:** Download [employees.pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/lab%20files/employees.pig.txt) . Open the file with an editor and replace the path of the employees.txt file in the employees.pig file as per your location and save the file.

**Apache Pig LIMIT Operator**

The Apache Pig `LIMIT` operator is used to limit the number of output tuples. 

**Apache Pig ORDER BY Operator**

The Apache Pig `ORDER BY` operator sorts a relation based on one or more fields. It maintains the order of tuples.

**Step 2:** Now, we will execute the script from the Grunt shell using the exec command as shown below: (replace the path with your file location)

```
exec d:/employees.pig.txt
```

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/images/pig_lab_5.png"  width="600">

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/images/pig_lab_6.png"  width="600">


You can see the Top 5 records being pulled out in Descending order as written in the script. Similarly, you can practice various Pig operators, for e.g. GROUP, SPLIT etc.

**This ends the exercise here.**























