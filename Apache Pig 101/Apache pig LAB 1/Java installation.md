![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/skillup.png)

#### JAVA DOWNLOAD:

**"Hadoop 3.0X supports only java 8"**

**Step 1:** To install Java we have to first download it. Click on [Download Java 8](https://www.oracle.com/in/java/technologies/javase/javase-jdk8-downloads.html) to proceed.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_1.png"  width="600">

**Step 2:** Scroll down and and choose the JDK which is meant for your Operating System
(Windows/Mac/Linux/Solaris).

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_2.png"  width="600">

**Step 3:** It asks you accept the Oracle License. Check the box to accept and click on
download.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_3.png"  width="600">

**Step 4:** After clicking on download option you will be directed to oracle login page. Create
your account and sign in. As you sign in, you will see Java 8 start downloading.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_4.png"  width="600">

**Step 5:**  Click on downloaded jdk file and select **“Next”**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_6.png"  width="600">

**Step 6:** : Change the location of installation if you wish. Else click on **“Next”**. This installs
Java on your system

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_7.png"  width="600">

#### Environment variable setting for java.

**Step 1:** Click on Start (or the windows button on your key board) and go to **Settings**.

**Step 2:** In Setting go to **“System”** and then search for **“Edit Environment variables for
your account”**.

**Step 3:** Select **“Environment variables”** and click ok.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_18.png"  width="600">

**Step 4:** Click on **“New”**.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_19.png"  width="600">

**Step 5:** Write variable name as: **“JAVA_HOME”** and value is the full path location of jdk
bin. For example: C:\Program Files\Java\jdk1.8.0_251\bin

**Step 6:** Under **System Variables**, select the variable **Path** and click on **Edit**.

**Step 7:** Open path, click on **“New”** and then paste the path location of jdk bin here also. Then click ok. Now you are done with setting up environment variables.


#### Verify the installation of Java.

Open Command prompt and type **“javac”** and enter.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_25.png"  width="600">

To know the version of java installed type **“java -version”**. It will appear like this.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_26.png"  width="600">

**Java has been successfully installed.**

                                                 **This ends the excercise**























