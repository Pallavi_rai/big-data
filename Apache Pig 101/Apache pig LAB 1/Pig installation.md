![s](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/skillup.png)

### Objective:
The objective of this lab is to get you started with Apache pig 101.

### Pre-requisites:

Java 8, Hadoop 3.x

### Pig Download:
**Step 1:** To download the Apache Pig, click [here](https://downloads.apache.org/pig/pig-0.17.0/pig-0.17.0.tar.gz).This will download pig-0.17.0.tar.gz file to your system.

**Step 2:**  Extract pig-0.17.0.tar.gz file using the appropriate tool for your OS.

**NOTE:** For Windows, you can use **WinRAR** to extract the file. It is available for download from this [link](https://www.win-rar.com/postdownload.html?&L=0) . For Mac use **The Unarchiver** from the app store. For Linux use `tar -xf hadoop-3.1.2.tar.gz` on the command prompt, from the directory where the file is located or giving the full path of the file.

If you are using WinRAR, click on extract file and a extraction path and options page will open as shown in the image below.


***NOTE:SAVE THE EXTRACTED PIG FILE IN HADOOP FOLDER ONLY***

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/pig_extract_1.png"  width="600">

**Step 3:** Pig is extracted in your system.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/pig_extract_2.png"  width="600">


**Step 4:** pig-0.17.0 is created in hadoop folder.**RENAME** pig-0.17.0 as pig.


### SETTING UP THE ENVIRONMENT VARIABLES FOR APACHE PIG:

**Step 1:** Click on start and go to **settings** --> In setting go to **“system”** and then search for **“edit for environment variables”** --> Select **“environment variables”** and click ok --> Click on **“New”** in User Variables.

Write variable name as: **“PIG_HOME”** and variable value is the path location of bin.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/PIG_4.png"  width="600">

**Step 2:** Then click on **“Path”** in system variables, select **“New”** enter the variable value of Pig bin folder here as well.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/PIG_5.png"  width="600">

### EDITION OF PIG FILES:

**Step 1:** GO to **pig folder** inside the hadoop folder> open bin > open pig.cmd file and edit it with Notepad++

edit here this=**HADOOP_BIN_PATH=C:\hadoop\libexec**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/pig_edit_1.png"  width="600">


**Step 2:** Go to C:\hadoop\libexec and open hadoop-config.cmd file and edit it with Notepad++

edit here this=**HADOOP_HOME=C:\hadoop\bin**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/pig_edit_2.png"  width="600">

###### Veryfying the installation of APACHE PIG.(COMMAND PROMPT)

**Step 1:** Open cmd and type command **"pig -version"**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/pig.cmd.png"  width="600">

**Step 2:** To run pig type command **"pig or pig -x local"**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/pig.cmd_2.png"  width="600">

You can see **Grunt shell** started.

**APACHE PIG IS RUNNING**

**In the next lab we will run some commands on "Apache Pig".**

                                                         **This Ends the Exercise.**
