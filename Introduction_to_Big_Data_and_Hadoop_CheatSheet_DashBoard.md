## CheatSheet - DashBoard

<center>
    <img src="https://gitlab.com/ibm/skills-network/courses/placeholder101/-/raw/master/labs/module%201/images/IDSNlogo.png" width="300" alt="cognitiveclass.ai logo"  />
</center>

<table>
<tr>
<th style="width:10%">
Command
</th>
<th style="width:30%">
Syntax
</th>
<th style="width:30%">
Description
</th>
<th style="width:30%">
Example
</th>

<tr>
<td style="width:10%">
docker-compose
</td>
<td style="width:30%">
<code> docker compose COMMAND</code>
</td>
<td style="width:30%">
Compose is a tool for defining and running multi-container Docker applications. It uses the YAML file to configure the serives and enables us to create and start all the services from just one configurtation file.
</td>
<td style="width:30%">
<code>
docker-compose up -d
</code>
</td>
</tr>

<tr>
<td style="width:10%">
docker-run
</td>
<td style="width:30%">
<code> docker run [OPTIONS] IMAGE [COMMAND] [ARG...]</code>
</td>
<td style="width:30%">
Run a command in a new container
</td>
<td style="width:30%">
<code>
docker run \
    --name spark-master \
    -h spark-master \
    -e ENABLE_INIT_DAEMON=false \
    -p 4040:4040 \
    -p 8080:8080 \
    -v `pwd`:/home/root \
    -d bde2020/spark-master:3.1.1-hadoop3.2
</code>
</td>
</tr>

<tr>
<td style="width:10%">
docker exec
</td>
<td style="width:30%">
<code>$ docker exec [OPTIONS] CONTAINER COMMAND [ARG...] </code>
</td>
<td style="width:30%">
The docker exec command runs a new command in a running container.
</td>
<td style="width:30%">
<code>
docker exec -it namenode /bin/bash
</code>
</td>
</tr>




<tr>
<td style="width:10%">
hdfs dfs
</td>
<td style="width:30%">
<code>hdfs dfs [COMMAND [COMMAND_OPTIONS]] </code>
</td>
<td style="width:30%">
Run a filesystem command on the file system supported in Hadoop. The various COMMAND_OPTIONS can be found at File System Shell Guide.
</td>
<td style="width:30%">
<code>
hdfs dfs -mkdir /user
</code>
</td>
</tr>

<tr>
<td style="width:10%">
hadoop jar
</td>
<td style="width:30%">
<code>hadoop jar <jar> [mainClass] args...</code>
</td>
<td style="width:30%">
Runs a jar file. Users can bundle their Map Reduce code in a jar file and execute it using this command.
</td>
<td style="width:30%">
<code>
hadoop jar $HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-examples-3.2.1.jar wordcount data.txt /user/root/output
</code>
</td>
</tr>

<tr>
<td style="width:10%">
pyspark
</td>
<td style="width:30%">
<code>from pyspark import <package-name> </code>
</td>
<td style="width:30%">
PySpark is the Spark API for Python.
</td>
<td style="width:30%">
<code>
from pyspark import SparkContext, SparkConf
</code>
</td>
</tr>


<tr>
<td style="width:10%">
SparkContext
</td>
<td style="width:30%">
<code>public SparkContext(String master,
                    String appName,
                    String sparkHome,
                    scala.collection.Seq<String> jars,
                    scala.collection.Map<String,String> environment)
 </code>
</td>
<td style="width:30%">
Alternative constructor that allows setting common Spark properties dirctly
</td>
<td style="width:30%">
<code>
sc = SparkContext("local", "first app")
</code>
</td>
</tr>

<tr>
<td style="width:10%">
pyspark.SparkContext.getOrCreate
</td>
<td style="width:30%">
<code>classmethod SparkContext.getOrCreate(conf=None)
 </code>
</td>
<td style="width:30%">
Get or instantiate a SparkContext and register it as a singleton object.
</td>
<td style="width:30%">
<code>
sc = SparkContext.getOrCreate();
</code>
</td>
</tr>


<tr>
<td style="width:10%">
Spark parallelize()
</td>
<td style="width:30%">
<code>public <T> RDD<T> parallelize(scala.collection.Seq<T> seq, int numSlices, scala.reflect.ClassTag<T> evidence$1)
 </code>
</td>
<td style="width:30%">
Spark parallelize() method creates N number of partitions if N is specified, else Spark would set N based on the Spark Cluster the driver program is running on.</td>
<td style="width:30%">
<code>
rdd = sc.parallelize([1,2,3,4,5,6,7,8,9,10])
</code>
</td>
</tr>


<tr>
<td style="width:10%">
spark.read.json
</td>
<td style="width:30%">
<code>
spark.read.json("path")
 </code>
</td>
<td style="width:30%">
To read a single line and multiline (multiple lines) JSON file into Spark DataFrame.
<td style="width:30%">
<code>
df = spark.read.json("people.json").cache()
</code>
</td>
</tr>

<tr>
<td style="width:10%">
stop()
</td>
<td style="width:30%">
<code>stop()</code>
</td>
<td style="width:30%">
Stop the underlying SparkContext.
</td>
<td style="width:30%">
<code>
spark = SparkSession.builder.appName("myApp").getOrCreate()
spark.stop()
</td>
</tr>

<tr>
<td style="width:10%">
pandas.read_csv
</td>
<td style="width:30%">
<code>pandas.read_csv(filepath)</code>
</td>
<td style="width:30%">
Read a comma-separated values (csv) file into DataFrame.
</td>
<td style="width:30%">
<code>
df = pandas.read_csv ('file_name.csv')
</code>
</td>
</tr>

<tr>
<td style="width:10%">
head()
</td>
<td style="width:30%">
<code>DataFrame.head(self, n=5)</code>
</td>
<td style="width:30%">
The head() function is used to get the first n rows.
</td>
<td style="width:30%">
<code>
data = pd.read_csv("filename.csv")
data.head()
</code>
</td>
</tr>

<tr>
<td style="width:10%">
createDataFrame()
</td>
<td style="width:30%">
<code>SparkSession.createDataFrame(data, schema=None, samplingRatio=None, verifySchema=True)</code>
</td>
<td style="width:30%">
Creates a DataFrame from an RDD, a list or a pandas.DataFrame.
</td>
<td style="width:30%">
<code>
rdd = sc.parallelize(l)
spark.createDataFrame(rdd).collect()
</code>
</td>
</tr>

<tr>
<td style="width:10%">
collect()
</td>
<td style="width:30%">
<code>RDD.collect()</code>
</td>
<td style="width:30%">
Return a list that contains all of the elements in the RDD.
</td>
<td style="width:30%">
<code>
rdd = sc.parallelize(l)
spark.createDataFrame(rdd).collect()
</code>
</td>
</tr>


<tr>
<td style="width:10%">
show()
</td>
<td style="width:30%">
<code>df.show(n, truncate=True)</code>
</td>
<td style="width:30%">
Prints the first n rows to the console.
</td>
<td style="width:30%">
<code>
df = spark.createDataFrame(data)
df.show(5) 
</code>
</td>
</tr>

<tr>
<td style="width:10%">
createTempView()
</td>
<td style="width:30%">
<code>DataFrame.createTempView(name)</code>
</td>
<td style="width:30%">
Creates a local temporary view with this DataFrame.
</td>
<td style="width:30%">
<code>
df.createTempView("people")
</code>
</td>
</tr>


<tr>
<td style="width:10%">
pandas_udf()
</td>
<td style="width:30%">
<code>pyspark.sql.functions.pandas_udf(f=None, returnType=None, functionType=None)</code>
</td>
<td style="width:30%">
Pandas UDFs are user defined functions that are executed by Spark using Arrow to transfer data and Pandas to work with the data, which allows vectorized operations.
</td>
<td style="width:30%">
<code>
import pandas as pd
from pyspark.sql.functions import pandas_udf
@pandas_udf(IntegerType())
</code>
</td>
</tr>


<tr>
<td style="width:10%">
kind
</td>
<td style="width:30%">
<code>curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.11.1/kind-linux-amd64
chmod +x ./kind</code>
</td>
<td style="width:30%">
Lets you run Kubernetes on your local computer. This tool requires that you have Docker installed and configured.
</td>
<td style="width:30%">
<code>
Put command in installation folder install_kind.sh and use:
./install_kind.sh
</code>
</td>
</tr>



<tr>
<td style="width:10%">
kubectl apply
</td>
<td style="width:30%">
<code>kubectl apply -f <filename></code>
</td>
<td style="width:30%">
apply manages applications through files defining Kubernetes resources. 
</td>
<td style="width:30%">
<code>
kubectl apply -f ./my-manifest.yaml
</code>
</td>
</tr>


<tr>
<td style="width:10%">
kubectl get
</td>
<td style="width:30%">
<code>kubectl get</code>
</td>
<td style="width:30%">
list resources
</td>
<td style="width:30%">
<code>
kubectl get pods
</code>
</td>
</tr>




<tr>
<td style="width:10%">
kubectl exec
</td>
<td style="width:30%">
<code>kubectl exec</code>
</td>
<td style="width:30%">
Execute a command on a container in a pod
</td>
<td style="width:30%">
<code>
kubectl exec -it spark -c spark  -- /bin/bash
</code>
</td>
</tr>


<tr>
<td style="width:10%">
kubectl logs
</td>
<td style="width:30%">
<code>kubectl logs ${POD_NAME} ${CONTAINER_NAME}
</code>
</td>
<td style="width:30%">
print the logs from a container in a pod
</td>
<td style="width:30%">
<code>
kubectl logs spark-pi-6f62d17a800beb3e-driver
</code>
</td>
</tr>








</table>


## Author(s)
Anamika


## Changelog

| Date       | Version | Changed by   | Change Description     |
| ---------- |-------- | ----------   | ---------------------  |
| 2020-08-26 | 1.0     | Anamika | Initial Version |