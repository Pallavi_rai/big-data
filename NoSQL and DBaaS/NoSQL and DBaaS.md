# Lab Exercise:Introducing NoSQL and Database-as-a-Service

## Contents 

Exercise 1. Signing Up for a Cloudant Account.

Exercise 2. Creating a Database and Adding Documents. 

Exercise 3. Replicating a Sample Database.

Exercise 4. Accessing the Cloudant API Using cURL. 

Exercise 5. Accessing the Cloudant API Using a Browser. 

## Exercise 1. Signing Up for a Cloudant Account 

#### What this exercise is about 

 This exercise shows you how to sign up for a Cloudant account and use your newly created credentials to log in to the Cloudant Dashboard.

#### What you should be able to do 

   At the end of this exercise, you should be able to:

  - Successfully sign up for a Cloudant account 

  - Log in to the Cloudant Dashboard 

#### Requirements 

 You need a notebook or desktop computer with a modern browser to connect to [IBM Cloud]( https://cloud.ibm.com/)

#### Exercise instructions

Follow these steps to complete this exercise:

1.	Log in to your IBM Cloud account.The IBM Cloud dashboard can be found at: [IBM Cloud](https://cloud.ibm.com/). After you authenticate with your user name and password, you're presented with the IBM Cloud dashboard. Click the ```Create resource``` button.

<img src="images/1.png"  width="600">

2.	Type ```Cloudant``` in the Search bar and click to open it:

<img src="images/2.png"  width="600">

3.	Select an offering and an environment.

<img src="images/3.png"  width="600">

4.	Enter an instance name. Verify that the resource group and authentication methods are correct. Add a tag if you like. The authentication methods that are available include ```IAM``` or ```IAM and legacy credentials```

**Note: Make sure you select the IAM and legacy credentials.**

<img src="images/4.png"  width="600">

5.	To create the service, click the ```Create``` button.

6.	After you click ```Create```, a message displays to say that the instance is being provisioned, which returns you to the Resource list. From the Resource list, you see the status for your instance is, "Provision in progress."

7.	When the status changes to Active, click the instance, then click the ```Service Credentials``` tab to create the connection information that your application needs to connect to the instance.

<img src="images/5.png"  width="600">

8.	Create an IBM Cloudant service credential: Click the ```New credential``` button.

<img src="images/6.png"  width="600">

9.	Follow the steps to create the credentials:

•	Enter a name for the new credential in the Add new credential window.

•	Accept the Manager role.

•	(Optional) Create a service ID or have one automatically generated for you.

•	(Optional) Add inline configuration parameters. This parameter isn't used by IBM Cloudant service credentials, so ignore it.

•	Click the Add button.

•	Your new credential appears after the table.

•	Click the chevron next to your credential.


<img src="images/7.png"  width="600">

<img src="images/8.png"  width="600">

10.	The details for the service credentials appear.

<img src="images/9.png"  width="600">

**End of exercise**


## Exercise 2. Creating a Database and Adding Documents

#### What this exercise is about 

 This exercise shows you how to use the Cloudant Dashboard to create a database and add documents. 

#### What you should be able to do 

 At the end of this exercise, you should be able to: 

• Navigate the Cloudant Dashboard. 

• Create a database. 

• Add documents in JSON format to the database. 

• Clone a document. 

• Delete a document. 

#### Requirements 
 You need a notebook or desktop computer with a modern browser to connect to the ```Cloudant``` Dashboard.

#### Exercise instructions

Follow these steps to complete this exercise: 

**Procedure 1: Creating a database**

1.	If you are still in IBM Cloud, Cloudant Instance, Go Manage.

<img src="images/10.png"  width="600">

2.	Click on **“Launch Dashboard”**.  This will open the **Cloudant Database.**

<img src="images/11.png"  width="600">

3.	Click on **“Create Database”** on Right-top corner. Write database name and click **“Create”.**

<img src="images/12.png"  width="600">

#### Procedure 2: Adding documents

1.	The database might take a few seconds to be created. You should then be on the database tab in the Cloudant Dashboard. Click the + sign next to All Documents and select New Doc to create a new document.

<img src="images/13.png"  width="600">

2.	Cloudant includes an easy-to-use JSON editor that helps ensure the JSON text is correctly formatted. Copy/paste the following text into the editor:

 { 

"_id": "bob_smith@ibm.com", 

"department": 23, 

"name": "Bob Smith",

 "title": "Developer",

"address": { 

"street": "123 Main St", 

"city": "Boston", 

"state": "MA"

 }, 

"skills": [

"Cloudant", 

"NoSQL", 

"JSON"

 ],

 "active_employee": true

 }

<img src="images/14.png"  width="600">

3.	Click **“Create Document”**. The document will be created, and you will be redirected to **“All Document”** page.

<img src="images/15.png"  width="600">

4.	To add another document to the database, you can click the + sign next to All Documents again, or alternatively click the Settings sign next to the database name as shown in the following image.

<img src="images/16.png"  width="600">

#### Procedure 3: Updating a document

1.	Now observe what happens to the _rev field when a document is updated. Edit the document you just created, add a phone number field, and click Save. See the following example, and notice that the _rev integer is incremented by one, and hash of the document body is now changed.

<img src="images/17.png"  width="600">

<img src="images/18.png"  width="600">

#### Procedure 4: Cloning a document

1.	With the document still open, click Clone Document in the top right corner.

2.	Provide a unique ID for the cloned document

<img src="images/19.png"  width="600">

3.	Change the name, title, address, and phone number for this new cloned document.

4.	Click Save, and then click Cancel.

#### Procedure 5: Deleting a document

1.	From the All Documents view, select one of the documents.

2.	In the header, click Select, then click the Trash icon as shown in the following image.

<img src="images/20.png"  width="600">

3.	When asked to confirm whether you want to delete the document, click OK to delete it.

**End of exercise**

## Exercise 3. Replicating a Sample Database 

#### What this exercise is about 

This exercise shows you how to replicate one of the sample databases from cloudant.com. 

#### What you should be able to do 

At the end of this exercise, you should be able to: 

 • Replicate a sample database. 

 • View the documents in the sample database.

#### Requirements 

You need a notebook or desktop computer with a modern browser to connect to ibm and the Cloudant Dashboard.
 
#### Exercise instructions 

Follow these steps to complete this exercise: 

#### Procedure 1: Replicating the database

1.	If you are still in IBM Cloud, Cloudant Instance, Go Manage.

2.	Click on “Launch Dashboard”.  This will open the Cloudant Database.

3.	In the Dashboard, select **“Replication”.**

<img src="images/21.png"  width="600">

4.	Click **“New Replication”.**

<img src="images/22.png"  width="600">

5.	Fill the Job Configuration as below:

<img src="images/23.png"  width="600">

<img src="images/24.png"  width="600">

**Note: In IAM Authentication enter the IAM apikey which can be obtained from service credentials.**

6.	Click **“Start Replication”**. The replication process has been started

<img src="images/25.png"  width="600">

<img src="images/26.png"  width="600">

**End of exercise**

##Exercise 4. Accessing the Cloudant API Using cURL 

#### What this exercise is about

 This exercise shows you how to use cURL to access the Cloudant HTTP API and perform basic tasks. 

#### What you should be able to do 

At the end of this exercise, you should be able to: 

•	Use cURL to access the Cloudant HTTP API. 

•	(Optional) Use jq to format JSON responses in a more readable format.

•	Get a list of databases in your account. 

•	Get a list of documents in a database. 

•	Create and delete documents using the API. 

#### Requirements 

You need a Linux shell with Internet access and the cURL utility installed. In windows we are using Cygwin terminal. (Optional) Although not required, it is helpful to format the JSON response by installing jq with cURL.

#### Exercise instructions 

Follow these steps to complete this exercise. 

#### Procedure 1: Viewing account information

1.	Open a command prompt, and verify that you can run the cURL command by typing 

**curl -V**

<img src="images/27.png"  width="600">

2.	Get information about your account by running the following command, replacing the parts in brackets (< >) with your account name. You will be prompted for your password. 

Command: **curl –X GET –u <username> '<url>'**

**Note: The username and url will be obtained from service credentials.**

<img src="images/28.png"  width="600">

#### Procedure 2: Getting a list of databases

1.	Get a list of databases in your account using the ‘_all_dbs’ endpoint. 

Command: **curl –s –u <username> '<url>/_all_dbs'**

<img src="images/29.png"  width="600">

#### Procedure 3: Creating a database

1.	Create a database called testdb using the PUT verb. 

Command: **curl –X PUT –u <username> '<url>/testdb'**

<img src="images/30.png"  width="600">

You can even check on the IBM Cloudant dashboard.

#### Procedure 4: Viewing and deleting documents

1.	View all documents using the ‘_all_docs’ endpoint. This is also known as the Primary Index for a database. The response includes a key, which is the _id of the document in the case of the Primary Index, and a value, which is the latest _rev of the document in the case of the Primary Index.

Command: **curl '<url>/businesscard/_all_docs'**

<img src="images/31.png"  width="600">

2.	Delete the document that you just created. You need to specify both the document _id and the latest _rev of the document to delete it. Obtain the _rev from the output of the previous step to get ‘_all_docs’.

Command: **curl –X DELETE '<url>/businesscard/<doc_id>?rev=<doc.rev>’**

**End of exercise**

## Exercise 5. Accessing the Cloudant API Using a Browser 

#### What this exercise is about 

This exercise shows you how to use a browser to access the Cloudant HTTP API and perform basic tasks. 

#### What you should be able to do

At the end of this exercise, you should be able to: 

•	(Optional) Use JSONView to format JSON responses in a more readable format. 

•	View your account information. 

•	Get a list of databases in your account. 

•	Get a list of documents in a database.

•	View a specific document by ID. 

#### Requirements

You need a desktop or laptop computer with Internet access, and Chrome or Firefox installed.

#### Exercise instructions 

Follow these steps to complete this exercise

1.	View your account information. 

URL: **<url>**

<img src="images/32.png"  width="600">

2.	View all the databases in your account. 

URL: **<url>_all_dbs**

<img src="images/33.png"  width="600">

3.	View all the documents in the businesscard database. 

URL: **<url>/businesscard/_all_docs**

<img src="images/34.png"  width="600">

4.	View the s_m@ibm.com document in the businesscard database. 

URL: **<url>/businesscard/s_m@ibm.com**

<img src="images/35.png"  width="600">

## Exercise 6. Working With the Cloudant API Using a Browser Add-On 

#### What this exercise is about 

This exercise shows you how to use a browser add-on tool to work with the Cloudant HTTP API and perform basic tasks. 

#### What you should be able to do

At the end of this exercise, you should be able to:

•	Use POSTMan to access the Cloudant HTTP API.

•	Get a list of databases in your account.

•	Get a list of documents in a database.

•	Create and delete documents by using the API.

#### Requirements 
You need a desktop or laptop computer with Internet access, and Chrome or Firefox installed. Use the following links to install these tools: [Dowmload](https://www.postman.com/downloads/)

#### Exercise instructions

Follow these steps to complete this exercise.

1.	Open the **“Postman”** app and launch a new tab

<img src="images/36.png"  width="600">

2.	View all the databases in your account. 

• URL: **<url>/_all_dbs** 

• Verb: **GET**

<img src="images/37.png"  width="600">

3.	View all the documents in the businesscard database. 

• URL: **<url>/businesscard/_all_docs**

• Verb: **GET**

<img src="images/38.png"  width="600">

<img src="images/39.png"  width="600">


4.	View the s_m@ibm.com document in the businesscard database.

• URL: **<url>businesscard/s_m@ibm.com**

• Verb: **GET**

<img src="images/40.png"  width="600">


5.	Create a new database called mynewdb. 

• URL: **<url>/mynewdb**

• Verb: **PUT**

<img src="images/41.png"  width="600">

6.	Create a new document in the mynewdb database. 

• URL: **<url>/mynewdb**

• Verb: **POST** 

• **Raw JSON**: 

  **{** 

   **"name": "William",**

   **"age": 50,**

   **"gender": "male",** 

   **"_id": "William"** 

  **}**

<img src="images/42.png"  width="600">

7.	Create two documents in bulk in the mynewdb database.

• URL: **<url>/mynewdb/_bulk_docs**

• Verb: **POST**

• **Raw JSON:** 

**{** 

**"docs" : [**

**{**

**"name": "Sarah",**

**"age": 43,**

**"gender": "female",**

**"_id": "sarah"**

**},**

**{**

**"name": "Nancy",**

**"age": 51,**

**"gender": "female",**

**"_id": "nancy"**

**}**

**]**

**}**

<img src="images/43.png"  width="600">

8.	Delete a document in the mynewdb database. 

• URL: **<url>/mynewdb/nancy?rev=1-b0f9d52c0df68f99db4e05 aed5033351**

• Verb: **DELETE**

<img src="images/44.png"  width="600">

                                              **End of exercise and Lab**













